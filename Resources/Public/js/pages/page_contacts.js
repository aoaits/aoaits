var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,				
				lat: 49.0437643,
				lng: 8.39504154
			  });
			  
			  var marker = map.addMarker({
				lat: 49.0437643,
				lng: 8.39504154,
	            title: 'Company, Inc.'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat : 49.0437643,
		        lng : 8.39504154
		      });
		    });
		}        

    };
}();